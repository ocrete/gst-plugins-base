
Release notes for GStreamer Base Plugins 1.1.4


The GStreamer team is proud to announce a new bug-fix release
in the 1.x stable series of the
core of the GStreamer streaming media framework.


The 1.x series is a stable series targeted at end users.
It is not API or ABI compatible with the stable 0.10.x series.
It is, however, parallel installable with the 0.10.x series and
will not affect an existing 0.10.x installation.



This module contains a set of reference plugins, base classes for other
plugins, and helper libraries. It also includes essential elements such
as audio and video format converters, and higher-level components like playbin,
decodebin, encodebin, and discoverer.

This module is kept up-to-date together with the core developments.  Element
writers should look at the elements in this module as a reference for
their development.

This module contains elements for, among others:

  device plugins: x(v)imagesink, alsa, v4lsrc, cdparanoia
  containers: ogg
  codecs: vorbis, theora
  text: textoverlay, subparse
  sources: audiotestsrc, videotestsrc, giosrc
  network: tcp
  typefind functions
  audio processing: audioconvert, adder, audiorate, audioresample, volume
  visualisation: libvisual
  video processing: videoconvert, videoscale
  high-level components: playbin, uridecodebin, decodebin, encodebin, discoverer
  libraries: app, audio, fft, pbutils, riff, rtp, rtsp, sdp, tag, video


Other modules containing plugins are:


gst-plugins-good
contains a set of well-supported plugins under our preferred license
gst-plugins-ugly
contains a set of well-supported plugins, but might pose problems for
    distributors
gst-plugins-bad
contains a set of less supported plugins that haven't passed the
    rigorous quality testing we expect, or are still missing documentation
    and/or unit tests
gst-libav
contains a set of codecs plugins based on libav (formerly gst-ffmpeg)



  

Bugs fixed in this release
     
      * 703378 : pbutils: Allow getting descriptions for unfixed caps
      * 660195 : GstDiscoverer: signals " discovered " and " finished " are only emitted in async mode
      * 553520 : tools: add gst-play utility that wraps playbin for testing purposes
      * 691462 : typefind: ADTS/AAC wrongly detected as MP2
      * 695889 : libs: many headers don't compile by themselves
      * 703405 : parse: Allow creating plain elements instead of single-element bins
      * 703669 : gst_rtcp_packet_fb_set_fci_length sets an incorrect buffer size.
      * 704929 : Prevent oggdemux from seeking a lot when the SEQUENTIAL scheduler flag is set
      * 705109 : audiodecoder/videodecoder: Needs to flush taglist on reception of stream-start
      * 705144 : mpegvideoparse: support field encoding for interlaced video
      * 705157 : tests: playbin_complex check fails during make check
      * 705369 : playbin: fix get-tags actions
      * 705555 : streamsynchronizer: should forward queries
      * 705826 : typefinding: add typefinder for webp imageformat
      * 705828 : videodecoder/videoencoder: Behaviour changes in ::reset() vfunc
      * 706061 : typefind: improved and extended typefinder for module music
      * 706569 : regression: playbin: chained oggs don't work any more
      * 706680 : dmabuf: fix mmap counting
      * 706885 : videoencoder: Fix forwarding of GstForceKeyUnit events

==== Download ====

You can find source releases of gst-plugins-base in the download
directory: http://gstreamer.freedesktop.org/src/gst-plugins-base/

The git repository and details how to clone it can be found at
http://cgit.freedesktop.org/gstreamer/gst-plugins-base/

==== Homepage ====

The project's website is http://gstreamer.freedesktop.org/

==== Support and Bugs ====

We use GNOME's bugzilla for bug reports and feature requests:
http://bugzilla.gnome.org/enter_bug.cgi?product=GStreamer

Please submit patches via bugzilla as well.

For help and support, please subscribe to and send questions to the
gstreamer-devel mailing list (see below for details).

There is also a #gstreamer IRC channel on the Freenode IRC network.

==== Developers ====

GStreamer is stored in Git, hosted at git.freedesktop.org, and can be cloned
from there (see link above).

Interested developers of the core library, plugins, and applications should
subscribe to the gstreamer-devel list.

        
Contributors to this release
    
      * Alessandro Decina
      * Andoni Morales Alastruey
      * Arnaud Vrac
      * Carlos Rafael Giani
      * David Schleef
      * Edward Hervey
      * Jie Yang
      * Jonathan Matthew
      * Lubosz Sarnecki
      * Matthieu Bouron
      * Michael Olbrich
      * Olivier Crête
      * Rico Tzschichholz
      * Sebastian Dröge
      * Sjoerd Simons
      * Sreerenj Balachandran
      * Thibault Saunier
      * Tim-Philipp Müller
      * Wim Taymans
      * Youness Alaoui
 